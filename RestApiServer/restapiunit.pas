unit RestApiUnit;

{$mode delphi}
{$H+}

interface

//-----------------------------------------------------------------------------------
// This query can be use from PHP, Java, Java script..etc
// for all front-end web applications
//-----------------------------------------------------------------------------------
//
// Restapi query example
//
// http://localhost:8080/api/example/info
// http://localhost:8080/api/example/sum?A=10&B=20
// http://localhost:8080/api/example/files?filename=xxx.json
// http://localhost:8080/api/example/variable
// http://localhost:8080/api/example/select
// http://localhost:8080/api/example/insert?somevalue=somestring

uses

  SysUtils,
  Classes,
  SuperObject,
  SqlDB,
  IBConnection,
  ibase60dyn,
  dynlibs;

const

//------------------------------------------------------------------------------
// RESTAPI SERVER CONFIG
//------------------------------------------------------------------------------

  //Enter your restapi server name
  RESTAPI_NAME      = 'Rest api example';

  //Enter your restapi version
  RESTAPI_VERSION   = '1.0.0.0';

  //Enter you restapi port
  RESTAPI_PORT      = 8080;

//------------------------------------------------------------------------------
// FIREBIRD DATABASE CONFIG
//------------------------------------------------------------------------------

  //Enter database server location
  DATABASE_HOSTNAME = 'localhost';

  //Enter database file location
{$ifdef windows}
  DATABASE_LOCATION = 'C:\ProgramData\restapi.fdb';
{$else}
  DATABASE_LOCATION = '/tmp/restapi.fdb';
{$endif}

  //Enter default username
  DATABASE_USERNAME = 'SYSDBA';

  //Enter default password
  DATABASE_PASSWORD = 'masterkey';

  //Enter defaut charset
  DATABASE_CHARSET  = 'UTF8';

//------------------------------------------------------------------------------
// RESTAPI IMPLEMENTATION FUNCTIONS
//------------------------------------------------------------------------------

//Restapi start
procedure RestApiStart(Query: TSqlQuery; Transaction: TSqlTransaction);

//Restapi implementation
function RestApiQuery(Header: ISuperObject; Body: ISuperObject; Response: ISuperObject; Query: TSqlQuery; Transaction: TSqlTransaction): Boolean;

//Restapi stop
procedure RestApiStop(Query: TSqlQuery; Transaction: TSqlTransaction);

implementation

procedure RestApiStart(Query: TSqlQuery; Transaction: TSqlTransaction);
begin
  randomize;

  try
    Query.SQL.Text:='CREATE TABLE TBL_EXAMPLE (TXT_FIELD CHAR(256));';
    Query.Prepare;
    Query.ExecSQL;
    Transaction.Commit;
  except
    //Allready exists
  end;


  Query.SQL.Text:='INSERT INTO TBL_EXAMPLE(TXT_FIELD) VALUES (:some_value);';
  Query.Prepare;
  Query.ParamByName('some_value').AsString:='autovalue'+IntToStr(Random(100));
  Query.ExecSQL;
  Transaction.Commit;

end;

procedure RestApiStop(Query: TSqlQuery; Transaction: TSqlTransaction);
begin

end;

function RestApiQuery(Header: ISuperObject; Body: ISuperObject; Response: ISuperObject; Query: TSqlQuery; Transaction: TSqlTransaction): Boolean;
begin
  Result:=False;

  //http://localhost:8080/api/example/info
  if Header.S['obj.link.path']='/api/example/info' then
  begin
    Result:=True;
    Response.S['response.restapi.name']:=RESTAPI_NAME;
    Response.S['response.restapi.version']:=RESTAPI_VERSION;
    Response.I['response.restapi.port']:=RESTAPI_PORT;
    Response.S['response.database.hostname']:=DATABASE_HOSTNAME;
    Response.S['response.database.location']:=DATABASE_LOCATION;
    Response.S['response.database.username']:=DATABASE_USERNAME;
    Response.S['response.database.password']:=DATABASE_PASSWORD;
    Response.S['response.database.charset']:=DATABASE_CHARSET;
    //Response status,code,description must be, it's standart of restapi!!!!
    Response.B['status']:=True;
    Response.I['code']:=200;
    Response.S['description']:='Success';
    Exit;
  end;

  //http://localhost:8080/api/example/sum?A=10&B=20
  if Header.S['obj.link.path']='/api/example/sum' then
  begin
    Result:=True;
    if not Assigned(Header.O['obj.link.value.a']) then raise Exception.Create('Value A not exists');
    if not Assigned(Header.O['obj.link.value.b']) then raise Exception.Create('Value B not exists');
    if Header.I['obj.link.value.a']>Header.I['obj.link.value.b'] then
    begin
      //Response status,code,description must be, it's standart of restapi!!!!
      Response.B['status']:=False;
      Response.I['code']:=501;
      Response.S['description']:='Error a > b';
    end
    else
    begin
      //Response status,code,description must be, it's standart of restapi!!!!
      Response.B['status']:=True;
      Response.I['code']:=200;
      Response.S['description']:='Success';
      Response.I['response.sum']:=Header.I['obj.link.value.a']+Header.I['obj.link.value.b'];
    end;
    Exit;
  end;

  //http://localhost:8080/api/example/files?filename=xxx.json
  if Header.S['obj.link.path']='/api/example/files' then
  begin
    Result:=True;
{$ifdef windows}
    Response.O['response']:=TSuperObject.ParseFile('C:\\ProgramData\'+Header.S['obj.link.value.filename'],false);
{$else}
    Response.O['response']:=TSuperObject.ParseFile('/tmp/'+Header.S['obj.link.value.filename'],false);
{$endif}
    Exit;
  end;

  //http://localhost:8080/api/example/variable
  if Header.S['obj.link.path']='/api/example/variable' then
  begin
    Result:=True;
    Response.S['response.a']:='Hello word';
    Response.I['response.b']:=10;
    Response.B['response.c']:=True;
    Response.D['response.d']:=10.5;
    Response.O['response.e']:=SO('[]');
    Response.A['response.e'].Add(10);
    Response.A['response.e'].Add(20);
    Response.A['response.e'].Add(30);
    //Response status,code,description must be, it's standart of restapi!!!!
    Response.B['status']:=True;
    Response.I['code']:=200;
    Response.S['description']:='Success';
    Exit;
  end;

  //http://localhost:8080/api/example/select
  if Header.S['obj.link.path']='/api/example/select' then
  begin
    Result:=True;
    Response.B['status']:=True;
    Response.I['code']:=200;
    Response.S['description']:='Success';
    Response.O['response.sometable']:=SO('[]');
    Query.SQL.Text:='SELECT * FROM TBL_EXAMPLE';
    Query.Open;
    Query.First;
    while Query.EOF=False do
    begin
      Response.A['response.sometable'].Add(Query.Fields.Fields[0].AsString);
      Query.Next;
    end;
    Query.Close;
    Exit;
  end;

  //http://localhost:8080/api/example/insert?somevalue=somestring
  if Header.S['obj.link.path']='/api/example/insert' then
  begin
    Result:=True;
    if not Assigned(Header.O['obj.link.value.somevalue']) then raise Exception.Create('Value somevalue not exists');
    Response.B['status']:=True;
    Response.I['code']:=200;
    Response.S['description']:='Success';
    try
      Query.SQL.Text:='INSERT INTO TBL_EXAMPLE(TXT_FIELD) VALUES (:some_value);';
      Query.Prepare;
      Query.ParamByName('some_value').AsString:=Header.S['obj.link.value.somevalue'];
      Query.ExecSQL;
      Transaction.Commit;
      Response.B['status']:=True;
      Response.I['code']:=200;
      Response.S['description']:='Success';
    except
      on E: Exception do
      begin
        Response.B['status']:=False;
        Response.I['code']:=500;
        Response.S['description']:=E.Message;
      end;
    end;
    Exit;
  end;


end;

end.

