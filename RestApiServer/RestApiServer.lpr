program RestApiServer;

{$mode objfpc}
{$H+}

uses
  {$ifdef unix}
    cthreads,
  {$endif}
  Sysutils,
  Classes,
  DateUtils,
  httpdefs,
  httproute,
  webutil,
  fphttpapp,
  syncobjs,
  SuperObject,
  RestApiUnit,
  SqlDB,
  IBConnection,
  ibase60dyn,
  dynlibs;

var

  Section: TCriticalSection;
  Connection: TIBConnection;
  Query: TSqlQuery;
  Transaction: TSqlTransaction;

function GetUrlObject(Url: String): ISuperObject;
  var l: Integer;
      n: Integer;
      m: Integer;
      q: Integer;
      b: String;
      s: String;
      d: String;
      k: String;
      O: ISuperObject;
      H: ISuperObject;
      P: ISuperObject;
      z: String;
      e: Integer;
  begin
    Url:=Trim(Url);
    if Url<>'' then
    begin
      b:=':/\?;#&';
      q:=0;
      s:='';
      try
        d:=HTTPDecode(Url);
      except
        d:=StringReplace(Url,'%','',[rfReplaceAll]);
      end;
      Result:=SO('{"raw":[]}');
      Result.S['org']:=Url;
      Result.S['url']:=d;
      if (Pos(':\',d)>0) or (Pos(':/',d)>0) then
      begin
        O:=SO;
        H:=SA([]);
        P:=SA([]);
        for n:=1 to Length(d) do
        begin
          if pos(d[n],b)=0 then
          begin
            s:=s+d[n];
          end
          else
          begin
            if d[n]=':' then Delete(b,1,1);
            if d[n]='?' then b:='&';
            if s<>'' then
            begin
              if q=0 then
              begin
                Result.A['raw'].Add(LowerCase(Trim(s)));
              end
              else
              begin
                if q=1 then
                begin
                  k:='';
                  for m:=1 to Length(S) do
                  begin
                    if pos(s[m],':@.')=0 then
                    begin
                      k:=k+s[m];
                    end
                    else
                    begin
                      if k<>'' then
                      begin
                        H.AsArray.Add(Trim(k));
                        k:='';
                      end;
                    end;
                  end;
                  if k<>'' then
                  begin
                    H.AsArray.Add(Trim(k));
                    k:='';
                  end;
                end
                else
                begin
                  l:=pos('=',s);
                  if l>0 then
                  begin
                    z:=StringReplace(Trim(LowerCase(Copy(s,1,l-1))),'[','',[rfReplaceAll, rfIgnoreCase]);
                    z:=StringReplace(z,']','',[rfReplaceAll, rfIgnoreCase]);
                    z:=StringReplace(z,'.','',[rfReplaceAll, rfIgnoreCase]);
                    O.S[z]:=Trim(Copy(s,l+1,Length(s)-l));
                  end
                  else
                  begin
                    P.AsArray.Add(Trim(s));
                  end;
                end;
              end;
              q:=q+1;
              s:='';
            end;
          end;
        end;
        if s<>'' then
        begin
          if q=0 then
          begin
            Result.A['raw'].Add(LowerCase(Trim(s)));
          end
          else
          begin
            if q=1 then
            begin
              k:='';
              for m:=1 to Length(S) do
              begin
                if pos(s[m],':@.')=0 then
                begin
                  k:=k+s[m];
                end
                else
                begin
                  if k<>'' then
                  begin
                    H.AsArray.Add(Trim(k));
                    k:='';
                  end;
                end;
              end;
              if k<>'' then
              begin
                H.AsArray.Add(Trim(k));
                k:='';
              end;
            end
            else
            begin
              l:=pos('=',s);
              if l>0 then
              begin
                z:=StringReplace(Trim(LowerCase(Copy(s,1,l-1))),'[','',[rfReplaceAll, rfIgnoreCase]);
                z:=StringReplace(z,']','',[rfReplaceAll, rfIgnoreCase]);
                z:=StringReplace(z,'.','',[rfReplaceAll, rfIgnoreCase]);
                O.S[z]:=Trim(Copy(s,l+1,Length(s)-l));
              end
              else
              begin
                P.AsArray.Add(Trim(s));
              end;
            end;
          end;
          q:=q+1;
          s:='';
        end;
        Result.A['raw'].Add(H);
        Result.A['raw'].Add(P);
        Result.A['raw'].Add(O);
      end
      else
      begin
        if Pos(';',d)>0 then
        begin
          for n:=1 to Length(d) do
          begin
            if d[n]<>';' then
            begin
              s:=s+d[n];
            end
            else
            begin
              if s<>'' then
              begin
                Result.A['raw'].Add(Trim(s));
                s:='';
              end;
            end;
          end;
          if s<>'' then
          begin
            Result.A['raw'].Add(s);
            s:='';
          end;
        end
        else
        begin
          for n:=1 to Length(d) do
          begin
            if d[n]<>' ' then
            begin
              s:=s+d[n];
            end
            else
            begin
              if s<>'' then
              begin
                Result.A['raw'].Add(s);
                s:='';
              end;
            end;
          end;
          if s<>'' then
          begin
            Result.A['raw'].Add(s);
            s:='';
          end;
        end;
      end;
      Result.I['dtm.dt']:=DateTimeToUnix(Now);
      Result.S['dtm.st']:=DateTimeToStr(UnixToDateTime(Result.I['dtm.dt']));
      if Result.A['raw'].Length>0 then
      begin
        Result.S['obj.link.protocol']:=LowerCase(Result.A['raw'].S[0]);

        if (Result.S['obj.link.protocol']='http') or
           (Result.S['obj.link.protocol']='https') then
        begin
          Result.S['obj.link.name']:='localhost';
          Result.S['obj.link.group']:='localhost';
          Result.S['obj.link.domain']:='localhost';
          if Result.S['obj.link.protocol']='http' then Result.I['obj.link.port']:=80 else Result.I['obj.link.port']:=443;
          Result.S['obj.link.path']:='/';
          if Result.A['raw'].Length>1 then
          begin
            if Result.A['raw'].O[1].AsArray.Length>2 then
            begin
              Result.S['obj.link.name']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]);
              Result.S['obj.link.group']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-3]);
              Result.S['obj.link.domain']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]+'.'+Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1]);
              for n:=Result.A['raw'].O[1].AsArray.Length-4 downto 0 do
              begin
                Result.S['obj.link.group']:=Result.S['obj.link.group']+'.'+LowerCase(Result.A['raw'].O[1].AsArray.S[n]);
              end;
              Result.S['obj.link.top']:=Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1];
            end
            else
            begin
              if Result.A['raw'].O[1].AsArray.Length=2 then
              begin
                Result.S['obj.link.name']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]);
                Result.S['obj.link.group']:='main';
                Result.S['obj.link.domain']:=LowerCase(Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-2]+'.'+Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1]);
                Result.S['obj.link.top']:=Result.A['raw'].O[1].AsArray.S[Result.A['raw'].O[1].AsArray.Length-1];
              end
              else
              begin
                if Result.A['raw'].O[1].AsArray.Length=1 then
                begin
                  Result.S['obj.link.name']:=LowerCase(Result.A['raw'].O[1].AsArray.S[0]);
                  Result.S['obj.link.group']:=Result.S['obj.link.name'];
                  Result.S['obj.link.domain']:=Result.S['obj.link.name'];
                  Result.S['obj.link.top']:=Result.S['obj.link.name'];
                end;
              end;
            end;
            if Result.A['raw'].Length>2 then
            begin
              if Result.A['raw'].O[2].AsArray.Length>0 then
              begin
                Result.S['obj.link.path']:='';
                for n:=0 to Result.A['raw'].O[2].AsArray.Length-1 do
                begin
                  Result.S['obj.link.path']:=Result.S['obj.link.path']+'/'+LowerCase(Result.A['raw'].O[2].AsArray.S[n]);
                end;
              end;
            end;
            if Result.A['raw'].Length=4 then
            begin
              Result.O['obj.link.value']:=Result.A['raw'].O[3];
              if Result.S['obj.option.typ']='' then
              begin
                if (Result.A['raw'].O[2].AsArray.Length>0) and
                   (pos('.',Result.A['raw'].O[2].AsArray.S[Result.A['raw'].O[2].AsArray.Length-1])>0) and
                   (pos(',',Result.A['raw'].O[2].AsArray.S[Result.A['raw'].O[2].AsArray.Length-1])=0) then
                begin
                  Result.S['obj.option.typ']:='file';
                  Result.S['obj.option.url']:=Result.S['org'];
                  Result.S['obj.option.dsc']:=Result.S['dsc'];
                  Result.S['obj.option.res']:=Result.A['raw'].O[2].AsArray.S[Result.A['raw'].O[2].AsArray.Length-1];
                  Result.S['obj.option.nam']:=Result.S['obj.option.res'];
                  Result.S['obj.option.ext']:=ExtractFileExt(Result.S['obj.option.res']);
                end
                else
                begin
                  Result.S['obj.option.typ']:='link';
                  Result.S['obj.option.url']:=Result.S['org'];
                  Result.S['obj.option.dsc']:=Result.S['dsc'];
                end;
              end;
            end;
          end;
        end
        else
        begin
          if Result.S['obj.link.protocol']='ftp' then
          begin
            Result.S['obj.link.name']:='localhost';
            Result.S['obj.link.group']:='localhost';
            Result.S['obj.link.domain']:='localhost';
            Result.I['obj.link.port']:=21;
            Result.S['obj.link.login']:='anonymous';
            Result.S['obj.link.password']:='anonymous';
            Result.S['obj.link.path']:='/';
            if Result.A['raw'].Length>1 then
            begin
              if Result.A['raw'].O[1].AsArray.Length=4 then
              begin

              end;
            end;
            //ftp://user:password@host:port/path
            if Result.A['raw'].Length>2 then
            begin
              for n:=0 to Result.A['raw'].O[2].AsArray.Length-1 do
              begin
                Result.S['obj.link.path']:=Result.S['obj.link.path']+'/'+LowerCase(Result.A['raw'].O[2].AsArray.S[n]);
              end;
            end;
          end
          else
          begin
            if Result.S['obj.link.protocol']='file' then
            begin
              if (Result.A['raw'].Length>3) and
                 (Result.A['raw'].O[1].AsArray.Length=1) then
              begin
                Result.S['obj.link.value.full']:=Result.A['raw'].O[1].AsArray.S[0]+':';
                for n:=0 to Result.A['raw'].O[2].AsArray.Length-1 do
                begin
                  Result.S['obj.link.value.full']:=Result.S['obj.link.value.full']+'\'+LowerCase(Result.A['raw'].O[2].AsArray.S[n]);
                end;
                Result.B['obj.link.value.exst']:=FileExists(Result.S['obj.link.value.full']);
                Result.S['obj.link.value.path']:=ExtractFilePath(Result.S['obj.link.value.full']);
                Result.S['obj.link.value.name']:=ExtractFileName(Result.S['obj.link.value.full']);
                Result.S['obj.link.value.extn']:=ExtractFileExt(Result.S['obj.link.value.full']);
              end;
            end;
          end;
        end;
      end;
    end
    else
    begin
      Result:=nil;
    end;
  end;

function GetBodyObject(Body: String): ISuperObject;
begin
  if Body<>'' then
  begin
    try
      Result:=SO(Body);
    except
      Result:=SO;
    end;
  end
  else
  begin
    Result:=SO;
  end;
end;

procedure DoRequestResponse(aReq: TRequest; aResp: TResponse);
var
  Header: ISuperObject;
  Body: ISuperObject;
  Response: ISuperObject;
begin
  try
    Section.Enter;
    Header:=GetUrlObject('http://'+aReq.Host+aReq.URL);
    Body:=GetBodyObject(aReq.Content);
    Response:=SO;
    try
      Response.Clear;
      if RestApiQuery(Header, Body, Response, Query, Transaction)=True then
      begin
        if Assigned(Response.O['status'])=False then Response.B['status']:=True;
        if Assigned(Response.O['code'])=False then Response.I['code']:=200;
        if Assigned(Response.O['description'])=False then Response.S['description']:='Success';
        AResp.Code:=Response.I['code'];
        AResp.CodeText:=Response.S['description'];
        AResp.ContentType:='application/json';
        AResp.Content:=Response.AsJSon;
      end
      else
      begin
        Response.B['status']:=False;
        Response.I['code']:=404;
        Response.S['description']:='Request not implemented ['+aReq.URL+']';
        AResp.Code:=404;
        AResp.CodeText:='Not found';
        AResp.ContentType:='application/json';
        AResp.Content:=Response.AsJSon;
      end;
    except
      on E: Exception do
      begin
        Response.B['status']:=False;
        Response.I['code']:=503;
        Response.S['description']:='Internal exception ['+E.Message+']';
        AResp.Code:=503;
        AResp.CodeText:='Internal error';
        AResp.ContentType:='application/json';
        AResp.Content:=Response.AsJSon;
      end;
    end;
  finally
    writeln('['+IntToStr(AResp.Code)+'] '+DateTimeToStr(Now)+' '+Header.S['org']+' - '+Response.S['description']);
    Section.Leave;
  end;
end;

begin
  writeln;
  writeln('==================================');
  writeln('Name: '+RESTAPI_NAME);
  writeln('Port: '+IntToStr(RESTAPI_PORT));
  writeln('Version: '+RESTAPI_VERSION);
  writeln('==================================');
  Section:=TCriticalSection.Create;
  Connection:=TIBConnection.Create(nil);
  Query:=TSqlQuery.Create(nil);
  Transaction:=TSqlTransaction.Create(nil);
  Connection.HostName:=DATABASE_HOSTNAME;
  Connection.DatabaseName:=DATABASE_LOCATION;
  Connection.UserName:=DATABASE_USERNAME;
  Connection.Password:=DATABASE_PASSWORD;
  Connection.CharSet:=DATABASE_CHARSET;
  try
    Connection.CreateDB;
    writeln('[ OK ] Prepare');
  except
    on E: Exception do
    begin
      writeln('[ OK ] Prepare');
    end;
  end;
  try
    Connection.Open;
    if Connection.Connected=True then
    begin
      writeln('[ OK ] Connected to database '+Connection.DatabaseName);
    end
    else
    begin
      writeln('[ ER ] Error connect to database '+Connection.DatabaseName);
    end;
  except
    on E: Exception do
    begin
      writeln('[ ER ] Error connect to database '+Connection.DatabaseName+'['+E.Message+']');
    end;
  end;
  if Connection.Connected=True then
  begin
    try
      Transaction.SQLConnection:=Connection;
      Query.SQLConnection:=Connection;
      Query.Transaction:=Transaction;
      writeln('[ OK ] Database is ready');
      RestApiStart(Query, Transaction);
    except
      on E: Exception do
      begin
        writeln('[ ER ] Database ready error ['+E.Message+']');
      end;
    end;
  end
  else
  begin
    writeln('[ ER ] Database is not ready');
  end;
  writeln;
  writeln('Press CTRL+C to exit');
  writeln;
  HTTPRouter.RegisterRoute('*', @DoRequestResponse);
  Application.Port:=RESTAPI_PORT;
  Application.Threaded:=True;
  Application.Initialize;
  Application.Run;
  RestApiStop(Query, Transaction);
  FreeAndNil(Transaction);
  FreeAndNil(Query);
  FreeAndNil(Connection);
  FreeAndNil(Section);
end.

