## Introduction

First of all start to use web application need to know next:

How to use json parser super object. It's is a best parser for FreePascal/Delphi
https://github.com/hgourvest/superobject

Little bit to know information about RestApi (page 1 and page 2 is enough)
https://www.tutorialspoint.com/restful/restful_web_services_tutorial.pdf

About default status codes of restapi
https://restfulapi.net/http-status-codes/

Note:
  a) Try to use defaut restapi status codes.
  b) Try to use lower case in json. Save you a lot of problems.

How to test restapi query:
You can use curl in linux or use online services like https://reqbin.com/

## Description of Web applicatrion RestApiServer (Fish)

Description files:
RestApiServer.lpi    - project file engine
RestApiServer.lpr    - project file engine
RestApiServer.lps    - project file engine
restapiunit.pas      - Implementation RestApiServer with firebird database
restapiunit.template - Template(fish) for your project. Looks like restapiunit.pas but without implementation.
suberobject.pas      - Json parser
superobject.pas      - Json parser
fbclient.dll         - Access to firebird for Windows. For linux need to install packets. You have installed already.

## Create firebird database on linux. Commands

1. Enter to directory /opt/firebird/bin
2. Enter command sudo ./isql 
3. Enter this magic words:

SQL>CREATE DATABASE '/tmp/restapi.fdb' page_size 8192
CON>user 'SYSDBA' password 'masterkey';
SQL>SELECT * FROM RDB$RELATIONS;
SQL>QUIT;

4. After this you can see restapi.fdb database

## About file restapiunit.pas

This file contain detailed description how is work. It's demo.

## Total

If you wanna make your implementation use template file. Change .template extension on .pas extension and build application.